<!DOCTYPE html>
<html>
<head>
	<title>REG</title>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<style type="text/css">
		body{
		    background-color: #525252;
		}
		.centered-form{
			margin-top: 60px;
		}

		.centered-form .panel{
			background: rgba(255, 255, 255, 0.8);
			box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
		}
	</style>
</head>
<body>
	<div class="container">
	    <div class="row centered-form">
	        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
	        	<div class="panel panel-default">
	        		<div class="panel-heading">
		    			<h3 class="panel-title">Please sign up <small>Pay Offline!</small></h3>
		 			</div>
			 		<div class="panel-body">
			    		<form role="form" action="signup.php" method="post" enctype="multipart/form-data">
			    			<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			                			<input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="Your Name" required="">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="number" name="doc_phone" id="doc_phone" class="form-control input-sm" placeholder="Phone Number" required="">
			    					</div>
			    				</div>
			    			</div>

			    			<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="email" name="doc_email" id="doc_email" class="form-control input-sm" placeholder="Email Address" required="">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="text" name="doc_dob" id="doc_dob" class="form-control input-sm" placeholder="DOB ( DD/MM/YYYY )" required="">
			    					</div>
			    				</div>
			    			</div>

			    			<div class="form-group">
			    				<input type="text" name="doc_address" id="doc_address" class="form-control input-sm" placeholder="Address" required="">
			    			</div>

			    			<div class="row">
			    				<div class="col-xs-4 col-sm-6 col-md-4">
			    					<div class="form-group">
			    						<input type="text" name="doc_city" id="doc_city" class="form-control input-sm" placeholder="City" required="">
			    					</div>
			    				</div>
			    				<div class="col-xs-4 col-sm-6 col-md-4">
			    					<div class="form-group">
			    						<input type="text" name="doc_state" id="doc_state" class="form-control input-sm" placeholder="State" required="">
			    					</div>
			    				</div>
			    				<div class="col-xs-4 col-sm-6 col-md-4">
			    					<div class="form-group">
			    						<input type="text" name="doc_country" id="doc_country" class="form-control input-sm" placeholder="Country" required="">
			    					</div>
			    				</div>
			    			</div>

			    			<div class="form-group">
			    				<input type="file" name="doc_profilepic" id="doc_profilepic" class="form-control input-sm" accept="image/gif, image/jpeg, image/png">
			    			</div>

			    			<div class="form-group">
			    				<input type="text" name="doc_profile" id="doc_profile" class="form-control input-sm" placeholder="Profile" required="">
			    			</div>

			    			<div class="form-group">
			    				<input type="text" name="doc_spec" id="doc_spec" class="form-control input-sm" placeholder="Speciality" required="">
			    			</div>

			    			<div class="form-group">
			    				<input type="text" name="doc_about" id="doc_about" class="form-control input-sm" placeholder="About You" required="">
			    			</div>
			    			
			    			<input type="submit" name="doc_signup" value="Register" class="btn btn-info btn-block">
			    		
			    		</form>
			    	</div>
		    	</div>
	    	</div>
	    </div>
	</div>
</body>
</html>