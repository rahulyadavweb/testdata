-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 05, 2019 at 11:29 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mpioc`
--

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

DROP TABLE IF EXISTS `signup`;
CREATE TABLE IF NOT EXISTS `signup` (
  `RegID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `DOB` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Address` text NOT NULL,
  `City` varchar(255) NOT NULL,
  `State` varchar(255) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `Reg_date` datetime NOT NULL,
  `Reg_number` varchar(500) NOT NULL,
  `Extra_f1` varchar(500) NOT NULL,
  `Extra_f2` varchar(500) NOT NULL,
  `Doc_profile` varchar(500) NOT NULL,
  `Doc_spec` text NOT NULL,
  `Payment` varchar(255) NOT NULL,
  `FoodCoupon` varchar(255) NOT NULL,
  PRIMARY KEY (`RegID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`RegID`, `Name`, `Email`, `DOB`, `Phone`, `Address`, `City`, `State`, `Country`, `Reg_date`, `Reg_number`, `Extra_f1`, `Extra_f2`, `Doc_profile`, `Doc_spec`, `Payment`, `FoodCoupon`) VALUES
(1, 're', 'rate@rer.com', '12/02/1989', '6782947943', '4444  Clement Street', 'Atlanta', 'Georgia', 'United States', '2019-03-05 16:33:11', 're#622695671', '', '', 'fgsg', 'Georgia', '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
